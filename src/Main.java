import java.util.Map;

public class Main {

	public static void main(String[] args) {
		Cache cache = Cache.getInstance();
		
		Map<EnumerationName,EnumValue> items;
		items.put(EnumerationName.Gender, new EnumValue(5, 5, "RE","Naprawa", EnumerationName.Gender));
		
		System.out.println(cache.getItems());
	}
}
