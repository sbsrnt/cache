import java.util.HashMap;
import java.util.Map;


public class Cache {
	public long lifetime;
	private Map<EnumerationName,EnumValue> items = new HashMap<EnumerationName,EnumValue>();
	
	private  static Cache instance;
	private static Object o = new Object();
	
	public static Update u;
	
	public static Cache getInstance(){ //singleton
		if (instance == null) {
            synchronized (o) {
                if (instance == null) {
                    instance = new Cache();
                }
            }
        }
		return instance;
	}
	
	
	public void clear() {
		items.clear();
	}
	
	public void add (EnumerationName key, EnumValue value) {
		items.put(key, value);
	}

	public long getLifetime() { 
		return lifetime; 
	}
	public void setLifetime(long lifetime) {
		this.lifetime = lifetime;
	}
	
	public Map<EnumerationName, EnumValue> getItems(){ 
		return this.items; 
	}
	
	public void setItems(Map<EnumerationName, EnumValue> items) { 
		this.items = items; 
	}

}
