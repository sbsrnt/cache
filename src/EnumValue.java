
public class EnumValue {
	private int id;
	private int enumId;
	private String code;
	private String value;
	private EnumerationName EnumerationName;

	public EnumValue(int id, int enumId, String code, String value, EnumerationName EnumerationName) {
		this.id = id;
		this.enumId = enumId;
		this.code = code;
		this.value = value;
		this.EnumerationName = EnumerationName;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEnumId() {
		return enumId;
	}
	public void setEnumId(int enumId) {
		this.enumId = enumId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public EnumerationName getEnumerationName() {
		return EnumerationName;
	}

	public void setEnumerationName(EnumerationName enumerationName) {
		EnumerationName = enumerationName;
	}
	
	@Override
	public String toString() {
		return "Enum value: " + id + "\nEnum ID: " + enumId + "\nEnum code: " + code + "\nEnum value: " + value
				+ "\nEnum name: " + EnumerationName;
	}
	
}
